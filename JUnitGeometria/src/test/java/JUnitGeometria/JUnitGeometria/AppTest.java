package JUnitGeometria.JUnitGeometria;

import dto.Geometria;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	
	public void testAreaCuadrado() {
		int resultado = Geometria.areacuadrado(2);
		int esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void testAreaCirculo() {
		double resultado = Geometria.areaCirculo(2);
		double esperado = 12.5664;
		assertEquals(esperado, resultado);
	}
	
	public void testAreaTriangulo() {
		int resultado = Geometria.areatriangulo(2, 2);
		int esperado = 2;
		assertEquals(esperado, resultado);
	}
	
	public void testAreaRectangulo() {
		int resultado = Geometria.arearectangulo(2, 2);
		int esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void testAreaPentagono() {
		int resultado = Geometria.areapentagono(2, 2);
		int esperado = 2;
		assertEquals(esperado, resultado);
	}
	
	public void testAreaRombo() {
		int resultado = Geometria.arearombo(2, 2);
		int esperado = 2;
		assertEquals(esperado, resultado);
	}
	
	public void testAreaRomboide() {
		int resultado = Geometria.arearomboide(2, 2);
		int esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void testAreaTrapecio() {
		int resultado = Geometria.areatrapecio(2, 2, 2);
		int esperado = 4;
		assertEquals(esperado, resultado);
	}
	
	public void testFigura() {
		String[] esperados = {"cuadrado", "Circulo", "Triangulo", "Rectangulo", "Pentagono", "Rombo", "Romboide", "Trapecio", "Default"};
		for (int i = 1; i <= 9; i++) {
			String resultado = Geometria.figura(i);
			String esperado = esperados[i-1];
			assertEquals(esperado, resultado);
		}
	}
}
